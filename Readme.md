# Installation #

## Silex and dependencies ##
php composer.phar install

## MySql  ##
Create a database and install required tables contained in conf/tables.sql by your own way (phpMyAdmin, mysql command ...)

## Settings ##
Copy config.inc.php and name it config.php
Update mysql credentials if needed 

## Test your Installation ##
http://localhost/XXXX/www/index.php/test
"OK" should appears 

# Test #
**Step 1:** parse the following wikipedia  https://fr.wikipedia.org/wiki/Liste_des_capitales_du_monde and extract for each country the capital and flag image and finally to store this information into the mysql table "capital" .
This operation will be executed when the user will call http://localhost/XXXX/www/index.php/parse and should be implemented in empty function Capital\Controller::parse  

**Step 2:** provide a basic web service.
http://localhost/XXXX/www/index.php/get should return an array formated in json with all fields stored in capital table. Implement Captital\Controller::getCapitalList function.


# Delivery #
zip the src/ folder and send it to me by email.