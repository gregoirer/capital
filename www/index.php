<?php

require_once(__DIR__."/../conf/config.php");
require_once(__DIR__."/../lib/rb.php");
$loader = require (__DIR__."/../vendor/autoload.php");
$loader->add('Capital', __DIR__.'/../src/');
$app = new Capital\Application($conf);
$app->boot();
$app->run();