<?php
namespace Capital;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

Class Controller{
    
    const PAGE_URL = "https://fr.wikipedia.org/wiki/Liste_des_capitales_du_monde";
    /**
     * parsePage 
     */
    function parsePage(Request $request, Application $appContainer){
        
    }
    
    function getCapitalList(Request $request, Application $appContainer){
        
    }
    
    function test(Request $request, Application $appContainer){
        $tables = \R::getCol("SHOW TABLES");
        $success = in_array("capital", $tables);
        return new Response($success ? "OK":"NOK");
    }
}
