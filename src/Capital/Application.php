<?php
namespace Capital;
use Silex\Application as BaseApplication;
use Silex\Provider\TwigServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Firewall\SimplePreAuthenticationListener;
use Symfony\Component\Security\Core\Authentication\Provider\SimpleAuthenticationProvider;
class Application extends BaseApplication{
        function __construct($conf){
            parent::__construct();
            //Redbean initialization 
            \R::setup("mysql:host=".$conf['db']['host'].";dbname=".$conf['db']['name'], $conf['db']['user'],  $conf['db']['password']);
          
            \R::setAutoResolve( false );
            $this->get("/get", "\Capital\Controller::getCapitalList");
            $this->get("/parse", "\Capital\Controller::parsePage");
            $this->get("/test", "\Capital\Controller::test");
        }
}